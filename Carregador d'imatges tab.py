'''
Este programa servix per carregar fitxers .tab de mapes de concentracions de
diversos elements a la superfície de Mart. Dades captades per la 2001 Mars Odissey i processats per
la Universitat d'Arizona.

Descàrrega: https://grs.lpl.arizona.edu/latestresults.jsp?lrid=25

Autor: Ausiàs Roch Talens
'''

# Càrrega de llibreries
import numpy as np
from PIL import Image
import shutil
from osgeo import gdal, osr

# Càrrega de les columnes
lat, lon, conc, sigma, sigma1 = np.loadtxt('Th_SR_5x5.tab', unpack = True)

# Especificar la resolució de les dades (s'asumïx un píxel quadrat)
res = 5
fil = 2*(max(lat)+res/2)/res
col = (max(lon)+res/2)/res

# Impressió de d'alguns paràmetres d'interés
print("Longitud de les dades:", len(conc), "píxels.")
print("Valor màxim:", max(conc), "ppm.")
print("Cantó superior esquerre (graus):", max(lat)+res/2,",", min(lon)-res/2)
print("Cantó inferior dret (graus):", min(lat)-res/2,",", max(lon)+res/2)
print("Tingueu en compte que la longitut marciana comprén de 0 a 360º.")
print("La imatge resultant tindrà una mida de ", fil,"per", col, "píxels.")

# Redimensionament de la matriu a la mida adequada
conc_redim = np.resize(conc, (int(fil), int(col)))


# Desa la matriu de les dades com a imatge
im = Image.fromarray(conc_redim)
im.save('Concentracions.tif')


# Georeferenciador
origen = 'Concentracions.tif'
eixida = 'Concentracionsref1.tif'

# Còpia l'arxiu i li canvia el nom
shutil.copy(origen, eixida)

# Obri l'arxiu d'eixida
ds = gdal.Open(eixida, gdal.GA_Update)

# Definix el sistema de referència
sr = osr.SpatialReference()
sr.ImportFromEPSG(4326) # Codi EPSG

# Introducció de punts de control
# 	Format: longitud, latitud, elevació, x, y
gcps = [gdal.GCP(-180, 90, 0, 0, 0),
gdal.GCP(-180, -90, 0, 0, 36),
gdal.GCP(180, -90, 0, 72, 36)]

sr1= "GEOGCS[\"Mars 2000\",DATUM[\"D_Mars_2000\",SPHEROID[\"Mars_2000_IAU_IAG\",3396190.0,169.89444722361179]],PRIMEM[\"Greenwich\",0],UNIT[\"Decimal_Degree\",0.0174532925199433]]"
# Aplica els punts de control
ds.SetGCPs(gcps, sr1)

# Tanca l'arxiu
ds = None
