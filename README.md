# Mars Odissey data parser
Este codi de Python servix per processar les dades de la Mars Odissey 2001 disponibles a la web de la Universitat d'Arizona:

En concret este codi fa el següent:
- Carrega l'arxiu .tab proporcionat per l'usuari.
- Processa les dades de l'arxiu i les reordena.
- Les desa en una imatge.
- Georeferència la imatge al sistema de referència marcià IAU-IAG Mars 2000.




